#Bakery application
This is the back-end of the bakery application

#Database
For this project we use a mongodb database, 
the database is running on port 27017. 
```
install mongodb community server
```
In a windows environment put the path to the bin folder in the path variable or go to the directory 
and execute the command
```
 mongod
```
While the database is running you can import some data 
```
mongo bakeryApi < usersJson.js
```
Personally I like to use MongoDB Compass Community to check the local mongodb database. 

#How to run the project
Note only the dev environment works at the moment.
```
npm install
ng run dev
```
