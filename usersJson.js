db.users.insert([
    {
        firstName: 'Tommy',
        lastName: 'Zhou',
        email: 'tommy.zhou@axxes.com',
        street: 'entrepotkaai',
        streetNr: '9',
        postCode: '2000',
        cityName: 'Antwerpen',
        purchases: [
            {
                purchaseName: 'Eclair',
                price: 4.5,
                date: '2019-10-07'
            },{
                purchaseName: 'Pie',
                price: 14.5,
                date: '2019-10-08'
            }
        ]
    },
    {
        firstName: 'Bram',
        lastName: 'Van Dyck',
        email: 'bram.vandyck@axxes.com',
        street: 'entrepotkaai',
        streetNr: '10A',
        postCode: '2000',
        cityName: 'Antwerpen',
        purchases: [
            {
                purchaseName: 'Croissant',
                price: 2.5,
                date: '2019-10-08'
            }
        ]
    }
]);
