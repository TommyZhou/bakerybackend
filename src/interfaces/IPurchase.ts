import {Document} from 'mongoose';

export interface IPurchase extends Document{
    purchaseName: string;
    price: number;
    date: Date;
}
