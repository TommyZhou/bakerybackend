import {Document} from 'mongoose';
import {IPurchase} from "./IPurchase";

export interface IUser extends Document{
    firstName: string;
    lastName: string;
    email: string;
    street: string;
    streetNr: string;
    postCode: string;
    cityName: string;
    purchases: IPurchase[];
}
