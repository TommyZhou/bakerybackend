import mongoose, {Schema} from "mongoose";
import {IUser} from "../interfaces/IUser";

const UserSchema = new mongoose.Schema(
    {
        _id:{
            type: Schema.Types.ObjectId,
        },
        firstName: {
            type: String,
            required: ['Please enter a firstName']
        },
        lastName: {
            type: String,
            required: ['Please enter a lastName']
        },
        email: {
            type: String,
            unique: true
        },
        street: {
            type: String
        },
        postCode: {
            type: String
        },
        cityName: {
            type: String
        },
        purchases: {
            type: Array
        }
    }
);

export default mongoose.model<IUser>('users', UserSchema);
