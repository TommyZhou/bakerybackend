import express from "express";
import logger from "./common/logger";
import helmet from "helmet";
import db from "./db";
import userRoute from "./routes/userRouter"
import cors from "cors"

const app = express();
app.use(cors({origin: "http://localhost:4200"}));
app.use(logger);
app.use(helmet());
//TODO use env variable
db("mongodb://localhost:27017/bakeryApi");

app.use("/api/users", userRoute);
export default app;
