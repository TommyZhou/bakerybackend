import morgan from "morgan";

const mode = process.env.NODE_ENV === "production" ? "combined" : "dev";
const httpLogger = morgan(mode);

export default httpLogger;
