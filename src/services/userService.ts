import {IUser} from "../interfaces/IUser";
import User from "../models/user"

export async function getAllUsers() {
    return User.find({}).then((data: IUser[]) => {
        return data;
    }).catch((error: Error) => {
        throw error;
    });
}

export async function getUserById(userId: string) {
    return User.findById(userId).then((data: IUser) => {
        return data;
    }).catch((error: Error) => {
        throw error;
    });
}
