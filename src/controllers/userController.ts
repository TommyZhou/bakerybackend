import {Request, Response} from 'express';
import * as userService from "../services/userService";

export async function getAllUsers(req: Request, res: Response) {
    const result = await userService.getAllUsers();
    res.status(200).json(result);
}

export async function getUserById(req: Request, res: Response) {
    const result = await userService.getUserById(req.params.userId);
    res.status(200).json(result);
}
